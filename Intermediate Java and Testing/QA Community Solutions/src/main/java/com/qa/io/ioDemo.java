package com.qa.io;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

public class ioDemo {

	public static void main(String[] args) throws FileNotFoundException {
		
		//in
		InputStream input = new FileInputStream("c:\\data\\inputTest.txt");

		int data = input.read();

		while(data != -1){
		  data = input.read();
		}	
		
			
		
		//out
		
		OutputStream output = new FileOutputStream("c:\\data\\outputTest.txt");
		output.write("Hello World".getBytes());
		output.close();
		
		
		
	}

}
